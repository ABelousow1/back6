<?php
$user = 'u20946';
$pass = '9860142';
try {
    $select_superpowers = '';
    $db = new PDO('mysql:host=localhost;dbname=u20946', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $sql = "select * from superpower order by sup_id";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $abilities = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($abilities as $row) {
        if (in_array($row['sup_id'], $superskills)) {
            $mes = 'selected';
        } else $mes = '';
        $select_superpowers.= sprintf('<option value="%s" %s>%s</option>',$row['sup_id'], $mes, $row['s_rusdesc']);
    }
    $radio_gender = '';
    $db = new PDO('mysql:host=localhost;dbname=u20946', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $sql = "select * from gender order by sex";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $gender = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($gender as $row) {
        if ($row['sex'] == $values['gender']) {
            $mes = 'checked';
        } else $mes = '';
        $radio_gender.= sprintf('<input id="g%s" type="radio" value="%s" name="gender" %s /><label for="g%s">%s</label>', $row['sex'], $row['sex'], $mes, $row['sex'], $row['rus_desc']);
    }
    $limbs_string = '';
    $db = new PDO('mysql:host=localhost;dbname=u20946', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $sql = "select * from amount_of_limbs order by limbs";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $limbs = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($limbs as $row) {
        if ($row['limbs'] == $values['limbs']){
            $mes = 'checked';
        } else $mes = '';
        $limbs_string.= sprintf('<input id="l%s" type="radio" value="%s" name="limbs" %s /><label for="l%s">%s</label>', $row['limbs'], $row['limbs'], $mes, $row['limbs'], $row['rus_desc']);
    }
}
catch (PDOException $e) {
    print('Error : ' . $e->getMessage());
    exit();
}?>
<!DOCTYPE html>
<html lang="ru">
<link rel="stylesheet" href="stylc1.css">
<body>
<?php
if (!empty($messages)) {
    print $messages['can_login'];
    if(!empty($_SESSION['login'])){
        printf($messages['login'], $_SESSION['login'], $_SESSION['uid']);
        echo '<a href="login.php?exit=1">Выход</a>';}
    else echo '<a href="login.php">Вход</a>';
}
?>
     <form class="railway" action="" method="POST">
            <div class="stripes-block">
            <div class="line"></div>
            </div>
                <?php if (!empty($messages)){
                    echo '
         <div class="forminfo">
                    ';
                    print $messages['res_saved'];
                    echo '
         </div>
                    ';
                }
                    ?>
            <div class="form-group">
            <label>
                Имя<br />
                <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
            </label><br />
            <?php if ($errors['fio']) {print $messages['fio'];} ?>

            <label>
                E-mail:<br />
                <input name="email"
                    <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
                       type="email" />
            </label><br />
            <?php if ($errors['email']) {print $messages['email'];} ?>

            <label>
                Дата рождения:<br />
                <input name="birthday"
                    <?php if ($errors['birthday']) {print 'class="error"';} ?> value="<?php print $values['birthday']; ?>"
                       type="date" min="1900-01-01" max="2022-12-31">
            </label><br />
            <?php if ($errors['birthday']) {print $messages['birthday'];} ?>
            <label>Пол:</label><br />
            <?php if ($errors['gender']) {print '<div class="error">';} ?>
                <?php echo $radio_gender?>
            <?php if ($errors['gender']) {print '</div>';print $messages['gender'];} ?>
            <div class="limbs">

            <label>Количество конечностей:</label><br />
            <?php if ($errors['limbs']) {print '<div class="error">';} ?>
                <?php  echo $limbs_string?>
            </div>
            <?php if ($errors['limbs']) {print '</div>';print $messages['limbs'];} ?>
            <?php echo (in_array("0",$superskills)) ?  "selected" : ""  ; ?>
            <label>
                Сверхспособности:
                <br />
                <select name="superskills[]"
                        multiple="multiple" <?php if ($errors['superskills']) {print 'class="error"';} ?>>
                    <?php echo $select_superpowers?>
                </select>
            </label><br />
            <?php if ($errors['superskills']) {print $messages['superskills'];} ?>

            <label>
                Биография:<br />
                <textarea name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php echo $values['biography'] ;  ?></textarea>
            </label><br />
            <?php if ($errors['biography']) {print $messages['biography'];} ?>
            <br /><input class="custom-checkbox" id="contract" type="checkbox"
                         name="agreement" <?php echo ($values['agreement']=='on') ? 'checked': '';?>/>
            <label class="checkbox" for="contract" <?php if ($errors['agreement']) {print 'class="error"';}?>>c контрактом ознакомлен

            </label><br />
            <?php if ($errors['agreement']) {print $messages['agreement'];} ?>
            <input type="hidden" name="token" value="<?php echo isset($_SESSION['token']) ?>">
            <input id="submit" type="submit" value="отправить" />
            </div>
        </form>
</body>

</html>
